const input = document.getElementById('input-id');
const div = document.getElementById('caption');
const err = document.getElementById('error');

input.addEventListener('focusout', (event) => {
    const num = parseInt(event.target.value);

  if (num >= 0) {

      div.innerHTML = `<span>Current price: ${num}</span> <button id="button-id">x</button>`
      input.classList.remove("error");
      const button = document.getElementById('button-id');
      err.innerHTML = "";
      button.addEventListener('click', event => {
         div.innerHTML = "";
      });
  } else {
      input.classList.add("error");
      err.innerHTML = "Please enter correct price!";
      div.innerHTML = "";
  }
  
});